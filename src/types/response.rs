use serde::Deserialize;

/// The response contains a JSON object, which always has a Boolean field 'ok' and may have an optional String field 'description' with a human-readable description of the result.
/// If 'ok' equals True, the request was successful and the result of the query can be found in the 'result' field. 
/// In case of an unsuccessful request, 'ok' equals false and the error is explained in the 'description'. 
/// An Integer 'error_code' field is also returned, but its contents are subject to change in the future.
///
#[derive(Deserialize, Clone, Debug)]
pub struct TelegramAPIResponse<T>
    where T: Sync + Clone
{
    pub ok: bool,
    pub description: Option<String>,
    pub result: Option<T>
}

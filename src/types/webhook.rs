use serde::{Serialize, Deserialize};


#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct WebhookInfo {
    /// Webhook URL, may be empty if webhook is not set up
    pub url: String,
    /// True, if a custom certificate was provided for webhook certificate checks
    pub has_custom_certificate: bool,
    /// Number of updates awaiting delivery
    pub pending_update_count: u64,
    /// Optional. Currently used webhook IP address
    pub ip_address: String,
    /// Optional. Unix time for the most recent error that happened when trying to deliver an update via webhook
    pub last_error_date: Option<u128>,
    /// Optional. Error message in human-readable format for the most recent error that happened when trying to deliver an update via webhook
    pub last_error_message: Option<String>,
    /// Optional. Unix time of the most recent error that happened when trying to synchronize available updates with Telegram datacenters
    pub last_synchronization_error_date: Option<u128>,
    /// Optional. The maximum allowed number of simultaneous HTTPS connections to the webhook for update delivery
    pub max_connections: Option<u64>,
    /// Optional. A list of update types the bot is subscribed to. Defaults to all update types except chat_member
    pub allowed_updates: Option<Vec<String>>
}

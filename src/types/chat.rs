use serde::{Serialize, Deserialize};

use super::message::Message;

#[derive(Serialize, Deserialize, Clone, Debug, Default)]
pub struct Chat {
    /// Unique identifier for this chat. This number may have more than 32 significant bits and some programming languages may have difficulty/silent defects in interpreting it. But it has at most 52 significant bits, so a signed 64-bit integer or double-precision float type are safe for storing this identifier.
    pub id: u64,
    /// Type of chat, can be either “private”, “group”, “supergroup” or “channel”
    #[serde(alias = "type")]
    pub type_of: String,
    /// Optional. Title, for supergroups, channels and group chats
    pub title: Option<String>,
    /// Optional. Username, for private chats, supergroups and channels if available
    pub username: Option<String>,
    /// Optional. First name of the other party in a private chat
    pub first_name: Option<String>,
    /// Optional. Last name of the other party in a private chat
    pub last_name: Option<String>,
    /// Optional. True, if the supergroup chat is a forum (has topics enabled)
    pub is_forum: Option<bool>,
    //// Optional. Chat photo. Returned only in getChat.
    // photo: Option<ChatPhoto>, // TODO
    /// Optional. If non-empty, the list of all active chat usernames; for private chats, supergroups and channels. Returned only in getChat.
    pub active_usernames: Option<Vec<String>>,
    /// Optional. Custom emoji identifier of emoji status of the other party in a private chat. Returned only in getChat.
    pub emoji_status_custom_emoji_id: Option<String>,
    /// Optional. Expiration date of the emoji status of the other party in a private chat in Unix time, if any. Returned only in getChat.
    pub emoji_status_expiration_date: Option<u128>,
    /// Optional. Bio of the other party in a private chat. Returned only in getChat.
    pub bio: Option<String>,
    /// Optional. True, if privacy settings of the other party in the private chat allows to use tg://user?id=<user_id> links only in chats with the user. Returned only in getChat.
    pub has_private_forwards: Option<bool>,
    /// Optional. True, if the privacy settings of the other party restrict sending voice and video note messages in the private chat. Returned only in getChat.
    pub has_restricted_voice_and_video_messages: Option<bool>,
    /// Optional. True, if users need to join the supergroup before they can send messages. Returned only in getChat.
    pub join_to_send_messages: Option<bool>,
    /// Optional. True, if all users directly joining the supergroup need to be approved by supergroup administrators. Returned only in getChat.
    pub join_by_request: Option<bool>,
    /// Optional. Description, for groups, supergroups and channel chats. Returned only in getChat.
    pub description: Option<String>,
    /// Optional. Primary invite link, for groups, supergroups and channel chats. Returned only in getChat.
    pub invite_link: Option<String>,
    /// Optional. The most recent pinned message (by sending date). Returned only in getChat.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub pinned_message: Option<Message>,
    //// Optional. Default chat member permissions, for groups and supergroups. Returned only in getChat.
    // permissions: Option<ChatPermissions>, // TODO
    /// Optional. For supergroups, the minimum allowed delay between consecutive messages sent by each unpriviledged user; in seconds. Returned only in getChat.
    pub slow_mode_delay: Option<u64>,
    /// Optional. The time after which all messages sent to the chat will be automatically deleted; in seconds. Returned only in getChat.
    pub message_auto_delete_time: Option<u64>,
    /// Optional. True, if aggressive anti-spam checks are enabled in the supergroup. The field is only available to chat administrators. Returned only in getChat.
    pub has_aggressive_anti_spam_enabled: Option<bool>,
    /// Optional. True, if non-administrators can only get the list of bots and administrators in the chat. Returned only in getChat.
    pub has_hidden_members: Option<bool>,
    /// Optional. True, if messages from the chat can't be forwarded to other chats. Returned only in getChat.
    pub has_protected_content: Option<bool>,
    /// Optional. For supergroups, name of group sticker set. Returned only in getChat.
    pub sticker_set_name: Option<String>,
    /// Optional. True, if the bot can change the group sticker set. Returned only in getChat.
    pub can_set_sticker_set: Option<bool>,
    /// Optional. Unique identifier for the linked chat, i.e. the discussion group identifier for a channel and vice versa; for supergroups and channel chats. This identifier may be greater than 32 bits and some programming languages may have difficulty/silent defects in interpreting it. But it is smaller than 52 bits, so a signed 64 bit integer or double-precision float type are safe for storing this identifier. Returned only in getChat.
    pub linked_chat_id: Option<u64>
    //// Optional. For supergroups, the location to which the supergroup is connected. Returned only in getChat.
    // location: Option<ChatLocation> // TODO
}

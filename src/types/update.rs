use serde::{Serialize, Deserialize};

use super::message::Message;

#[derive(Serialize, Deserialize, Clone, Debug, Default)]
pub struct Update {
    /// The update's unique identifier. Update identifiers start from a certain positive number and increase sequentially. This ID becomes especially handy if you're using webhooks, since it allows you to ignore repeated updates or to restore the correct update sequence, should they get out of order. If there are no new updates for at least a week, then identifier of the next update will be chosen randomly instead of sequentially.
    pub update_id: u64,
    /// Optional. New incoming message of any kind - text, photo, sticker, etc.
    pub message: Option<Message>,
    /// Optional. New version of a message that is known to the bot and was edited
    pub edited_message: Option<Message>,
    /// Optional. New incoming channel post of any kind - text, photo, sticker, etc.
    pub channel_post: Option<Message>,
    // Optional. New version of a channel post that is known to the bot and was edited
    pub edited_channel_post: Option<Message>,
    //// Optional. New incoming inline query
    // inline_query: Option<InlineQuery>, // TODO
    //// Optional. The result of an inline query that was chosen by a user and sent to their chat partner. Please see our documentation on the feedback collecting for details on how to enable these updates for your bot.
    // chosen_inline_result: Option<ChosenInlineResult>, // TODO
    //// Optional. New incoming callback query
    // callback_query: Option<CallbackQuery>, // TODO
    //// Optional. New incoming shipping query. Only for invoices with flexible price
    // shipping_query: Option<ShippingQuery>, // TODO
    //// Optional. New incoming pre-checkout query. Contains full information about checkout
    // pre_checkout_query: Option<PreCheckoutQuery>, // TODO
    //// Optional. New poll state. Bots receive only updates about stopped polls and polls, which are sent by the bot
    // poll: Option<Poll>, // TODO
    //// Optional. A user changed their answer in a non-anonymous poll. Bots receive new votes only in polls that were sent by the bot itself.
    // poll_answer: Option<PollAnswer>, // TODO
    //// Optional. The bot's chat member status was updated in a chat. For private chats, this update is received only when the bot is blocked or unblocked by the user.
    // my_chat_member: Option<ChatMemberUpdated>, // TODO
    //// Optional. A chat member's status was updated in a chat. The bot must be an administrator in the chat and must explicitly specify “chat_member” in the list of allowed_updates to receive these updates.
    // chat_member: Option<ChatMemberUpdated>, // TODO
    //// Optional. A request to join the chat has been sent. The bot must have the can_invite_users administrator right in the chat to receive these updates.
    // chat_join_request: Option<ChatJoinRequest> // TODO
}

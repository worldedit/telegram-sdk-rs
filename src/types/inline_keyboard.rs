use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct InlineKeyboardMarkup {
    /// Array of button rows, each represented by an Array of InlineKeyboardButton objects
    pub inline_keyboard: Vec<Vec<InlineKeyboardButton>>
}

#[derive(Serialize, Deserialize, Clone, Debug, Default)]
/// This object represents one button of an inline keyboard. You must use exactly one of the optional fields.
pub struct InlineKeyboardButton {
    /// Label text on the button
    pub text: String,
    /// Optional. HTTP or tg:// URL to be opened when the button is pressed. Links tg://user?id=<user_id> can be used to mention a user by their ID without using a username, if this is allowed by their privacy settings.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub url: Option<String>,
    /// Optional. Data to be sent in a callback query to the bot when button is pressed, 1-64 bytes
    pub callback_data: Option<String>,

    //// Optional. Description of the Web App that will be launched when the user presses the button. The Web App will be able to send an arbitrary message on behalf of the user using the method answerWebAppQuery. Available only in private chats between a user and the bot.
    // web_app: Option<WebAppInfo>, // TODO

    //// Optional. An HTTPS URL used to automatically authorize the user. Can be used as a replacement for the Telegram Login Widget.
    // login_url: Option<LoginUrl>, // TODO

    /// Optional. If set, pressing the button will prompt the user to select one of their chats, open that chat and insert the bot's username and the specified inline query in the input field. May be empty, in which case just the bot's username will be inserted.
    pub switch_inline_query: Option<String>,
    /// Optional. If set, pressing the button will insert the bot's username and the specified inline query in the current chat's input field. May be empty, in which case only the bot's username will be inserted.
    pub switch_inline_query_current_chat: Option<String>,

    //// Optional. If set, pressing the button will prompt the user to select one of their chats of the specified type, open that chat and insert the bot's username and the specified inline query in the input field
    //// This offers a quick way for the user to open your bot in inline mode in the same chat - good for selecting something from multiple options.
    // switch_inline_query_chosen_chat: Option<SwitchInlineQueryChosenChat>, // TODO

    //// Optional. Description of the game that will be launched when the user presses the button.
    //// NOTE: This type of button must always be the first button in the first row.
    // callback_game: Option<CallbackGame>, // TODO

    /// Optional. Specify True, to send a Pay button.
    /// NOTE: This type of button must always be the first button in the first row and can only be used in invoice messages.
    pub pay: Option<bool>,
}

impl InlineKeyboardButton {
    pub fn new () -> Self {
        Self { ..Default::default() }
    }

    pub fn from_text (text: &str) -> Self {
        Self {
            text: text.to_string(),
            ..Default::default()
        }
    }

    pub fn with_callback_data (&mut self, data: &str) -> &mut Self {
        self.callback_data = Some(data.to_string()); self
    }
}

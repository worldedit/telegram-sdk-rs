use serde::{Serialize, Deserialize};

use super::{user::User, chat::Chat, inline_keyboard::InlineKeyboardMarkup};

#[derive(Serialize, Deserialize, Clone, Debug, Default)]
pub struct Message {
    pub message_id: u64,
    pub message_thread_id: Option<u64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub from: Option<User>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub sender_chat: Option<Box<Chat>>,
    pub date: u128,
    pub chat: Box<Chat>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub forward_from: Option<User>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub forward_from_chat: Option<Box<Chat>>,
    pub forward_from_message_id: Option<u64>,
    pub forward_signature: Option<String>,
    pub forward_sender_name: Option<String>,
    pub forward_date: Option<u64>,
    pub is_topic_message: Option<bool>,
    pub is_automatic_forward: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub reply_to_message: Option<Box<Self>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub via_bot: Option<User>,
    pub edit_date: Option<u64>,
    /// True, if the message can't be forwarded
    pub has_protected_content: Option<bool>,
    /// Optional. The unique identifier of a media message group this message belongs to
    pub media_group_id: Option<String>,
    /// Optional. Signature of the post author for messages in channels, or the custom title of an anonymous group administrator
    pub author_signature: Option<String>,
    /// Optional. For text messages, the actual UTF-8 text of the message
    pub text: Option<String>,
    //// Array of MessageEntity	Optional. For text messages, special entities like usernames, URLs, bot commands, etc. that appear in the text
    // entities: Vec<MessageEntity>, // TODO
    //// Animation	Optional. Message is an animation, information about the animation. For backward compatibility, when this field is set, the document field will also be set
    // animation: Option<Animation>, // TODO
    //// Audio	Optional. Message is an audio file, information about the file
    // audio: Option<Audio>, // TODO
    //// Document	Optional. Message is a general file, information about the file
    // document: Option<Document>, // TODO
    //// Array of PhotoSize	Optional. Message is a photo, available sizes of the photo
    // photo: Option<Vec<PhotoSize>>, // TODO
    //// Sticker	Optional. Message is a sticker, information about the sticker
    // sticker: Option<Sticker>, // TODO
    //// Story	Optional. Message is a forwarded story
    // story: Option<Story>, // TODO
    //// Video	Optional. Message is a video, information about the video
    // video: Option<Video>, // TODO
    //// VideoNote	Optional. Message is a video note, information about the video message
    // video_note: Option<VideoNote>, // TODO
    //// Voice	Optional. Message is a voice message, information about the file
    // voice: Option<Voice>, // TODO
    /// String	Optional. Caption for the animation, audio, document, photo, video or voice
    pub caption: Option<String>,
    /// Array of MessageEntity	Optional. For messages with a caption, special entities like usernames, URLs, bot commands, etc. that appear in the caption
    pub caption_entities: Option<Vec<MessageEntity>>, // TODO
    /// True	Optional. True, if the message media is covered by a spoiler animation
    pub has_media_spoiler: Option<bool>,
    //// Contact	Optional. Message is a shared contact, information about the contact
    // contact: Option<Contact>, // TODO
    //// Dice	Optional. Message is a dice with random value
    // dice: Option<Dice>, // TODO
    //// Game	Optional. Message is a game, information about the game. More about games »
    // game: Option<Game>, // TODO
    //// Poll	Optional. Message is a native poll, information about the poll
    // poll: Option<Poll>, // TODO
    //// Venue	Optional. Message is a venue, information about the venue. For backward compatibility, when this field is set, the location field will also be set
    // venue: Option<Venue>, // TODO
    //// Location	Optional. Message is a shared location, information about the location
    // location: Option<Location>, // TODO
    /// Array of User	Optional. New members that were added to the group or supergroup and information about them (the bot itself may be one of these members)
    pub new_chat_members: Option<Vec<User>>,
    /// User	Optional. A member was removed from the group, information about them (this member may be the bot itself)
    pub left_chat_member: Option<User>,
    /// String	Optional. A chat title was changed to this value
    pub new_chat_title: Option<String>,
    //// Array of PhotoSize	Optional. A chat photo was change to this value
    // new_chat_photo: Option<Array<PhotoSize>>, // TODO
    /// True	Optional. Service message: the chat photo was deleted
    pub delete_chat_photo: Option<bool>,
    /// True	Optional. Service message: the group has been created
    pub group_chat_created: Option<bool>,
    /// True	Optional. Service message: the supergroup has been created. This field can't be received in a message coming through updates, because bot can't be a member of a supergroup when it is created. It can only be found in reply_to_message if someone replies to a very first message in a directly created supergroup.
    pub supergroup_chat_created: Option<bool>,
    /// True	Optional. Service message: the channel has been created. This field can't be received in a message coming through updates, because bot can't be a member of a channel when it is created. It can only be found in reply_to_message if someone replies to a very first message in a channel.
    pub channel_chat_created: Option<bool>,
    //// MessageAutoDeleteTimerChanged	Optional. Service message: auto-delete timer settings changed in the chat
    // message_auto_delete_timer_changed: Option<MessageAutoDeleteTimerChanged>, // TODO

    /// Integer	Optional. The group has been migrated to a supergroup with the specified identifier. This number may have more than 32 significant bits and some programming languages may have difficulty/silent defects in interpreting it. But it has at most 52 significant bits, so a signed 64-bit integer or double-precision float type are safe for storing this identifier.
    pub migrate_to_chat_id: Option<u64>,
    /// Integer	Optional. The supergroup has been migrated from a group with the specified identifier. This number may have more than 32 significant bits and some programming languages may have difficulty/silent defects in interpreting it. But it has at most 52 significant bits, so a signed 64-bit integer or double-precision float type are safe for storing this identifier.
    pub migrate_from_chat_id: Option<u64>,
    //// Message	Optional. Specified message was pinned. Note that the Message object in this field will not contain further reply_to_message fields even if it is itself a reply.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub pinned_message: Option<Box<Message>>,
    //// Invoice	Optional. Message is an invoice for a payment, information about the invoice. More about payments »
    // invoice: Option<Invoice>, // TODO
    //// SuccessfulPayment	Optional. Message is a service message about a successful payment, information about the payment. More about payments »
    // successful_payment: Option<SuccessfulPayment> // TODO
    //// UserShared	Optional. Service message: a user was shared with the bot
    // user_shared: Option<UserShared>, // TODO
    //// ChatShared	Optional. Service message: a chat was shared with the bot
    // chat_shared: Option<ChatShared>, // TODO
    /// Optional. The domain name of the website on which the user has logged in. More about Telegram Login »
    pub connected_website: Option<String>,
    //// Optional. Service message: the user allowed the bot added to the attachment menu to write messages
    // write_access_allowed: Option<WriteAccessAllowed	, // TODO
    //// Optional. Telegram Passport data
    // passport_data: Option<PassportData>, // TODO
    //// Optional. Service message. A user in the chat triggered another user's proximity alert while sharing Live Location.
    // proximity_alert_triggered: Option<ProximityAlertTriggered>, // TODO
    //// Optional. Service message: forum topic created
    // forum_topic_created: Option<ForumTopicCreated>, // TODO
    //// Optional. Service message: forum topic edited
    // forum_topic_edited: Option<ForumTopicEdited>, // TODO
    //// Optional. Service message: forum topic closed
    // forum_topic_closed: Option<ForumTopicClosed>, // TODO
    //// Optional. Service message: forum topic reopened
    // forum_topic_reopened: Option<ForumTopicReopened>, // TODO
    //// Optional. Service message: the 'General' forum topic hidden
    // general_forum_topic_hidden: Option<GeneralForumTopicHidden>, // TODO
    //// Optional. Service message: the 'General' forum topic unhidden
    // general_forum_topic_unhidden: Option<GeneralForumTopicUnhidden>, // TODO
    //// Optional. Service message: video chat scheduled
    // video_chat_scheduled: Option<VideoChatScheduled>, // TODO
    //// Optional. Service message: video chat started
    // video_chat_started: Option<VideoChatStarted>, // TODO
    //// Optional. Service message: video chat ended
    // video_chat_ended: Option<VideoChatEnded>, // TODO
    //// Optional. Service message: new participants invited to a video chat
    // video_chat_participants_invited: Option<VideoChatParticipantsInvited>, // TODO
    //// Optional. Service message: data sent by a Web App
    // web_app_data: Option<WebAppData>, // TODO
    /// Optional. Inline keyboard attached to the message. login_url buttons are represented as ordinary url buttons.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub reply_markup: Option<InlineKeyboardMarkup> // TODO
}

/// This object represents one special entity in a text message. For example, hashtags, usernames, URLs, etc.
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct MessageEntity {
    /// Type of the entity. Currently, can be “mention” (@username), “hashtag” (#hashtag), “cashtag” ($USD), “bot_command” (/start@jobs_bot), “url” (https://telegram.org), “email” (do-not-reply@telegram.org), “phone_number” (+1-212-555-0123), “bold” (bold text), “italic” (italic text), “underline” (underlined text), “strikethrough” (strikethrough text), “spoiler” (spoiler message), “code” (monowidth string), “pre” (monowidth block), “text_link” (for clickable text URLs), “text_mention” (for users without usernames), “custom_emoji” (for inline custom emoji stickers)
    #[serde(alias = "type")]
    pub type_of: String,
    /// Offset in UTF-16 code units to the start of the entity
    pub offse: u64,
    /// Length of the entity in UTF-16 code units
    pub length: u64,
    /// Optional. For “text_link” only, URL that will be opened after user taps on the text
    pub url: Option<String>,
    /// Optional. For “text_mention” only, the mentioned user
    pub user: Option<User>,
    /// Optional. For “pre” only, the programming language of the entity text
    pub language: Option<String>,
    /// Optional. For “custom_emoji” only, unique identifier of the custom emoji. Use `getCustomEmojiStickers` to get full information about the sticker
    pub custom_emoji_id: Option<String>,
}

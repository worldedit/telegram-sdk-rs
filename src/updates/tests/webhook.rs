use log::info;

use crate::{client::{TAPI, APIClient}, updates::{query::{delete_webhook::DeleteWebhookQuery, set_webhook::SetWebhookQuery}, self}, test_env::bot_token};

#[tokio::test]
pub async fn test_webhook () {
    env_logger::try_init().ok();
    log::set_max_level(log::LevelFilter::Info);

    let mut client = crate::client::APIClient::new();
    client.with_api_token(&bot_token());
    // let api = crate::methods::MethodsHandler::init(&client);

    test_set_remove_webhook(&client).await;
}

pub async fn test_set_remove_webhook(client: &APIClient) {
    let set_query = SetWebhookQuery {
        url: "https://0xr.in/test".to_string(),
        ..Default::default()
    };
    let delete_query = DeleteWebhookQuery {
        drop_pending_updates: Some(true),
        ..Default::default()
    };
    // SET WEBHOOK
    let result = updates::set_webhook(&client, &set_query).await;
    info!("[test_set_remove_webhook] set result: {:#?}", result);
    assert_eq!(result.is_ok(), true);

    // WEBHOOK INFO
    let result = updates::get_webhook_info(&client).await;
    info!("[test_set_remove_webhook] info result: {:#?}", result);
    assert_eq!(result.is_ok(), true);

    // DELETE WEBHOOK
    let result = updates::delete_webhook(&client, &delete_query).await;
    info!("[test_set_remove_webhook] delete result: {:#?}", result);
    assert_eq!(result.is_ok(), true)
}

use log::info;

use crate::{client::{TAPI, APIClient}, test_env::bot_token, updates};

#[tokio::test]
pub async fn test_fetchers () {
    env_logger::try_init().ok();
    log::set_max_level(log::LevelFilter::Info);

    let mut client = crate::client::APIClient::new();
    client.with_api_token(&bot_token());
    // let api = crate::methods::MethodsHandler::init(&client);

    test_bot_get_updates(&client).await;
}

pub async fn test_bot_get_updates(client: &APIClient) {
    let result = updates::get_updates(&client).await;
    info!("[test_bot_get_updates] result: {:#?}", result);
    assert_eq!(result.is_ok(), true)
}

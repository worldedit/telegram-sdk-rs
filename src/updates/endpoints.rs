static GET_UPDATES: &str = "getUpdates";
static SET_WEBHOOK: &str = "setWebhook";
static DELETE_WEBHOOK: &str = "deleteWebhook";
static GET_WEBHOOK_INFO: &str = "getWebhookInfo";

pub struct Endpoint { }

impl Endpoint {
    pub fn e(v: &str) -> String { v.to_string() }

    pub fn get_updates() -> String { Self::e(GET_UPDATES)}
    pub fn set_webhook() -> String { Self::e(SET_WEBHOOK)}
    pub fn delete_webhook() -> String { Self::e(DELETE_WEBHOOK)}
    pub fn get_webhook_info() -> String { Self::e(GET_WEBHOOK_INFO)}
}

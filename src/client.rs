// use log::info;
use reqwest::{Url, Method};
use serde::{Serialize, Deserialize, de::DeserializeOwned};

use crate::error::{self, APIError};

pub static DEFAULT_API_URL: &str = "https://api.telegram.org";

pub trait TAPI {
    fn new() -> Self;
    fn with_api_url (&mut self, api_url: &str) -> &mut Self;
    fn with_api_token (&mut self, api_token: &str) -> &mut Self;
    fn make_url (&self, endpoint: &str) -> Result<Url, url::ParseError>;
    fn api_token (&self) -> String;
}


#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct APIClient {
    pub api_url: String,
    pub api_token: Option<String>,
}

// impl APIClient { }

impl TAPI for APIClient {
    /** Initialize client */
    fn new () -> Self {
        Self {
            api_url: DEFAULT_API_URL.to_string(),
            api_token: None
        }
    }

    fn with_api_url (&mut self, api_url: &str) -> &mut Self {
        self.api_url = api_url.to_string(); self
    }

    fn with_api_token (&mut self, api_token: &str) -> &mut Self {
        self.api_token = Some(api_token.to_string()); self
    }

    fn make_url (&self, endpoint: &str) -> Result<Url, url::ParseError> {
        let url = format!("{}/bot{}/{}", &self.api_url, &self.api_token(), endpoint);
        Url::parse(&url)
    }

    fn api_token (&self) -> String {
        // let token = format!("Bearer {}", self.read_token_key);
        match &self.api_token {
            Some(v) => v.to_string(),
            None => "".to_string()
        }
    }
}

type E = error::APIError;

impl APIClient {
    pub async fn regular_post<T, B>(
        &self,
        endpoint: &str,
        request_body: Option<&B>
    ) -> Result<T, E>
        where T: DeserializeOwned + Sync,
        B: Serialize + Sync
    {
        let url = self.make_url(endpoint).unwrap();

        let client  = reqwest::Client::new();

        // info!("body is {:#?}", serde_json::to_string_pretty(&request_body));
        let builder = client
            .request(Method::POST, url)
            .json(&request_body);
        let request = builder.build().unwrap();

        // self.client.set_auth_header_read_token(&mut request);
        let mut err = APIError::unknown();

        let result = reqwest::Client::new()
            .execute(request)
            .await;
        if result.is_err() {
            let err: APIError = result.unwrap_err().into();
            return Err(err);
        }
        let body = result.unwrap().text().await;
        if body.is_err() {
            err.with_msg("Parse response body error");
            return Err(err)
        }
        let d = body.unwrap();
        match serde_json::from_str(&d) {
            Ok(v) => Ok(v),
            Err(e) => {
                let msg = format!("Serde deserialize response body error:  {}", &e.to_string());
                err.with_msg(&msg);
                err.with_detail(&d);
                Err(err)
            }
        }
    }
}

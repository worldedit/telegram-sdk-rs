pub fn chat_id () -> u64 {
    std::env::var("TG_TEST_CHAT_ID")
        .unwrap()
        .parse()
        .unwrap()
}

pub fn bot_token () -> String {
    std::env::var("TG_TEST_BOT_TOKEN")
        .unwrap()
}

use serde::{Serialize, Deserialize};

use crate::types::{message::MessageEntity, inline_keyboard::InlineKeyboardMarkup, reply_keyboard::{ReplyKeyboardMarkup, ReplyKeyboardRemove, ForceReply}};

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(untagged)]
pub enum MessageChatId { U64(u64), String(String) }

impl Default for MessageChatId {
    fn default() -> Self {
        MessageChatId::String("".to_string())
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, Default)]
pub struct SendMessageQuery {
    /// Yes Unique identifier for the target chat or username of the target channel (in the format @channelusername)
    pub chat_id: MessageChatId,
    /// Optional	Unique identifier for the target message thread (topic) of the forum; for forum supergroups only
    pub message_thread_id: Option<u64>,
    /// Yes	Text of the message to be sent, 1-4096 characters after entities parsing
    pub text: String,
    /// Optional	Mode for parsing entities in the message text. See formatting options for more details.
    pub parse_mode: Option<String>,
    /// Optional	A JSON-serialized list of special entities that appear in message text, which can be specified instead of parse_mode
    #[serde(skip_serializing_if = "Option::is_none")]
    pub entities: Option<Vec<MessageEntity>>,
    /// Optional	Disables link previews for links in this message
    #[serde(skip_serializing_if = "Option::is_none")]
    pub disable_web_page_preview: Option<bool>,
    /// Optional	Sends the message silently. Users will receive a notification with no sound.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub disable_notification: Option<bool>,
    /// Optional Protects the contents of the sent message from forwarding and saving
    pub protect_content: Option<bool>,
    /// Optional	If the message is a reply, ID of the original message
    #[serde(skip_serializing_if = "Option::is_none")]
    pub reply_to_message_id: Option<u64>,
    ///// Optional	Pass True if the message should be sent even if the specified replied-to message is not found
    pub allow_sending_without_reply: Option<bool>,
    //// InlineKeyboardMarkup or ReplyKeyboardMarkup or ReplyKeyboardRemove or ForceReply	Optional	Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub reply_markup: Option<SendMessageQueryReplyMarkup> // TODO
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(untagged)]
pub enum SendMessageQueryReplyMarkup {
    InlineKeyboardMarkup(InlineKeyboardMarkup),
    ReplyKeyboardMarkup(ReplyKeyboardMarkup),
    ReplyKeyboardRemove(ReplyKeyboardRemove),
    ForceReply(ForceReply)
}

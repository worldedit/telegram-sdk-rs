static GET_ME: &str = "getMe";
static SEND_MESSAGE: &str = "sendMessage";

pub struct Endpoint { }

impl Endpoint {
    pub fn e(v: &str) -> String { v.to_string() }

    pub fn get_me() -> String { Self::e(GET_ME)}
    pub fn send_message() -> String { Self::e(SEND_MESSAGE)}
}

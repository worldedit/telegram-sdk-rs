use log::info;

use crate::{client::{TAPI, APIClient}, test_env::bot_token, methods};

#[tokio::test]
pub async fn test_fetchers () {
    env_logger::try_init().ok();
    log::set_max_level(log::LevelFilter::Info);

    let mut client = crate::client::APIClient::new();
    client.with_api_token(&bot_token());
    test_bot_get_me(&client).await;
}

pub async fn test_bot_get_me(client: &APIClient) {
    let result = methods::get_me(&client).await;
    info!("[test_bot_get_me] result: {:#?}", result);
    assert_eq!(result.is_ok(), true)
}

use log::info;

use crate::{methods::{query::send_message::{SendMessageQuery, MessageChatId, SendMessageQueryReplyMarkup}, self}, client::{TAPI, APIClient}, types::{inline_keyboard::{InlineKeyboardMarkup, InlineKeyboardButton}, reply_keyboard::ReplyKeyboardMarkup, keyboard::KeyboardButton}, test_env::{chat_id, bot_token}};


#[tokio::test]
pub async fn test_msg () {
    env_logger::try_init().ok();
    log::set_max_level(log::LevelFilter::Info);

    let mut client = crate::client::APIClient::new();
    client.with_api_token(&bot_token());

    test_send_msg(&client).await;
    // test_send_msg_reply_kbd(&client).await;
}

pub async fn test_send_msg(client: &APIClient) {
    let data = SendMessageQuery {
        chat_id: MessageChatId::U64(chat_id()),
        text: "Some testing text is there".to_string(),
        parse_mode: Some("Markdown".to_string()),
        ..Default::default()
    };
    let result = methods::send_message(&client, &data).await;
    info!("[test_send_msg] result: {:#?}", result);
    assert_eq!(result.is_ok(), true)
}

pub async fn test_send_msg_inline_kbd(client: &APIClient) {
    let reply_markup = SendMessageQueryReplyMarkup::InlineKeyboardMarkup(
        InlineKeyboardMarkup {
            inline_keyboard: vec![
                vec![
                    InlineKeyboardButton::from_text("Choose 1")
                        .with_callback_data("choose_1_selection")
                        .to_owned()
                ]
            ]
        }
    );
    info!("reply is {:#?}", serde_json::to_string_pretty(&reply_markup).ok());
    let data = SendMessageQuery {
        chat_id: MessageChatId::U64(chat_id()),
        text: "With inline keyboard".to_string(),
        parse_mode: Some("Markdown".to_string()),
        reply_markup: Some(reply_markup),
        ..Default::default()
    };
    let result = methods::send_message(&client, &data).await;
    info!("[test_send_msg_inline_kbd] result: {:#?}", result);
    assert_eq!(result.is_ok(), true)
}

pub async fn test_send_msg_reply_kbd(client: &APIClient) {
    let reply_markup = SendMessageQueryReplyMarkup::ReplyKeyboardMarkup(
        ReplyKeyboardMarkup {
            keyboard: vec![
                vec![
                    KeyboardButton::from_text("Choose 1")
                        .to_owned()
                ],
            ],
            ..Default::default()
        }
    );
    info!("reply is {:#?}", serde_json::to_string_pretty(&reply_markup).ok());
    let data = SendMessageQuery {
        chat_id: MessageChatId::U64(chat_id()),
        text: "With reply keyboard".to_string(),
        parse_mode: Some("Markdown".to_string()),
        reply_markup: Some(reply_markup),
        ..Default::default()
    };
    let result = methods::send_message(&client, &data).await;
    info!("[test_send_msg_reply_kbd] result: {:#?}", result);
    assert_eq!(result.is_ok(), true)
}

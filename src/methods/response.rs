use serde::{Deserialize, Serialize};

use super::types;

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct GetCurrencyRatesResult {
    #[serde(rename(deserialize = "currencyRates"))]
    pub currency_rates: Vec<types::CurrencyRate>
}

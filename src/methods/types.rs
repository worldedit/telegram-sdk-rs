use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct CurrencyRate {
    pub symbol: String,
    #[serde(rename(deserialize="isFiat"))]
    pub is_fiat: bool,
    pub price: f64
}

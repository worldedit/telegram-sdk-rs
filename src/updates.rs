use crate::{client::APIClient, types::{response::TelegramAPIResponse, update::Update, webhook::WebhookInfo}, error::{self}};

use self::{endpoints::Endpoint, query::{set_webhook::SetWebhookQuery, delete_webhook::DeleteWebhookQuery}};

pub mod endpoints;
pub mod query;

#[cfg(test)]
pub mod tests;

type E = error::APIError;
type R<T> = TelegramAPIResponse<T>;

pub async fn get_updates(
    client: &APIClient
) -> Result<R<Vec<Update>>, E> {
    client.regular_post::<R<Vec<Update>>, ()>(
        &Endpoint::get_updates(),
        None
    ).await
}

pub async fn set_webhook(
    client: &APIClient,
    data: &SetWebhookQuery
) -> Result<R<bool>, E> {
    client.regular_post::<R<bool>, SetWebhookQuery>(
        &Endpoint::set_webhook(),
        Some(data)
    ).await
}

pub async fn delete_webhook(
    client: &APIClient,
    data: &DeleteWebhookQuery
) -> Result<R<bool>, E> {
    client.regular_post::<R<bool>, DeleteWebhookQuery>(
        &Endpoint::delete_webhook(),
        Some(data)
    ).await
}

pub async fn get_webhook_info(
    client: &APIClient,
) -> Result<R<WebhookInfo>, E> {
    client.regular_post::<R<WebhookInfo>, ()>(
        &Endpoint::get_webhook_info(),
        None
    ).await
}

use crate::{error::{self}, client::APIClient, methods::endpoints::Endpoint, types::{user::User, response::TelegramAPIResponse, message::Message} };

#[cfg(test)]
pub mod tests;

pub mod types;
pub mod response;
pub mod endpoints;
pub mod query;

type E = error::APIError;
type R<T> = TelegramAPIResponse<T>;

pub async fn get_me(
    client: &APIClient
) -> Result<R<User>, E> {
    client.regular_post::<R<User>, ()>(
        &Endpoint::get_me(), None
    ).await
}

pub async fn send_message(
    client: &APIClient,
    data: &query::send_message::SendMessageQuery
) -> Result<R<Message>, E> {
    client.regular_post::<R<Message>, query::send_message::SendMessageQuery>(
        &Endpoint::send_message(), Some(data)
    ).await
}

use core::fmt;

use serde::Serialize;

#[derive(Clone, Debug, Serialize)]
pub enum APIErrorKind {
    ReqwestError,
    Unknown
}

#[derive(Clone, Debug, Serialize)]
pub struct APIError {
    pub kind: APIErrorKind,
    pub msg: String,
    pub detail: String,
    pub data: Option<String>
}

impl APIError {
    pub fn new(kind: APIErrorKind) -> Self { Self { kind, msg: "".to_string(), detail: "".to_string(), data: None } }
    pub fn with_msg(&mut self, msg: &str) -> &mut Self { self.msg = msg.to_string(); self }
    pub fn with_detail(&mut self, detail: &str) -> &mut Self { self.detail = detail.to_string(); self }
    pub fn with_data(&mut self, data: &str) -> &mut Self { self.data = Some(data.to_string()); self }

    pub fn unknown() -> Self { Self::new(APIErrorKind::Unknown) }
}

impl fmt::Display for APIError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "[APIError]. kind: {:#?}, msg: {}", self.kind, self.msg)
    }
}

impl From<reqwest::Error> for APIError {
    fn from(value: reqwest::Error) -> Self {
        let msg = value.to_string();
        let url: String = match value.url() {
            Some(v) => v.to_string(),
            None => "".to_string()
        };
        let status: String = match value.status() {
            Some(v) => v.to_string(),
            None => "".to_string()
        };
        let detail: String = format!("url: {}  status: {}", url, status);
        Self {
            kind: APIErrorKind::ReqwestError,
            msg,
            detail,
            data: None,
        }
    }
}
